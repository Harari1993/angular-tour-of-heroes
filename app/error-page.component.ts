import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'error-page',
    template: `<div class="error">Error</div>`,
    styleUrls:['./error-page.component.css']
})
export class ErrorPageComponent {
}